<?php
namespace Src\Units\Handlers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Src\Units\Exceptions\BaseException;
use Src\Units\Exceptions\ValidationException;
use Laminas\Diactoros\Response;

/**
 * Class CustomExceptionHandler to format errors depending on the request and type
 * @package Src\Units\Handlers
 */
class CustomExceptionHandler
{
    /**
     * @param ServerRequestInterface $request
     * @param \Exception $error
     * @throws \Exception
     */
    public function handleError(ServerRequestInterface $request, \Exception $error): void
	{
        /* The router will return the 422 */
        if ($error instanceof ValidationException) {
            $this->unprocessableEntity($error->getMessage());
        }

        /* The router will throw the 4xx */
		if ($error instanceof BaseException) {
			response()->httpCode($error->getCode())->json([
                'error' => $error->getMessage(),
                'code'  => $error->getCode(),
            ]);
		}
		throw $error;
	}

    /**
     * @param string $message
     * @return ResponseInterface
     */
    public function unprocessableEntity(string $message): ResponseInterface
    {
        $response = new Response;
        $httpCode = 422;
        $error = json_decode($message, true);
        $body = [
            'httpCode' => $httpCode,
            'validation' => $error['validation'],
            'data' => $error['data'],
            'message' => 'Invalid data!',
        ];
        $response->getBody()->write(json_encode($body));
        return $response->withStatus($httpCode);
    }

}
