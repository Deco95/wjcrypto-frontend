<?php

namespace Src\Units\Handlers;

use Src\Units\Exceptions\BaseException;

/**
 * Class SessionHandler
 * @package Src\Units\Handlers
 */
class SessionHandler
{
    /**
     * Session constructor.
     * @param bool $auto_start
     */
    public function __construct(bool $auto_start = false)
    {
        if ($auto_start) {
            $this->start();
        }
    }

    /**
     * @throws \Exception
     */
    public function handle(): void
    {
        if (!$this->isStarted()) {
            throw new BaseException('The session isn\'t started', 500);
        }
    }

    /**
     * @return void
     */
    public function start(): void
    {
        if (!$this->isStarted()) {
            session_start();
        }
    }

    /**
     * @return bool
     */
    public function isStarted(): bool
    {
        return isset($_SESSION);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function all(): array
    {
        $this->handle();
        return $_SESSION;
    }

    /**
     * @param string $key
     * @return mixed
     * @throws \Exception
     */
    public function read(string $key)
    {
        $this->handle();

        if (isset($this->all()[$key])) {
            return $this->all()[$key];
        } else {
            return null;
        }
    }

    /**
     * @param string $key
     * @param mixed $value
     * @throws \Exception
     */
    public function write(string $key, $value): void
    {
        $this->handle();
        $_SESSION[$key] = $value;
    }

    /**
     * @param string $key
     * @throws \Exception
     */
    public function remove(string $key): void
    {
        $this->handle();
        unset($_SESSION[$key]);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function allFlash(): array
    {
        $this->handle();
        $data = $this->read('flash');

        if (!is_array($data) || $data === null) {
            $data = [];
        }

        return $data;
    }

    /**
     * @param string $key
     * @return mixed
     * @throws \Exception
     */
    public function readFlash(string $key)
    {
        $this->handle();

        if (isset($this->read('flash')[$key])) {
            return $this->read('flash')[$key];
        } else {
            return null;
        }
    }

    /**
     * @param string $key
     * @param mixed $value
     * @throws \Exception
     */
    public function writeFlash(string $key, $value): void
    {
        $this->handle();

        if ($this->handleWriteFlash($key)) {
            return;
        }

        $data = $this->allFlash();

        $flashData = $data;

        $flashData[$key] = $value;
//        $flashData[$key]['timestamp'] = microtime();

        $this->write('flash', $flashData);
    }

    /**
     * @param string $key
     * @throws \Exception
     */
    protected function removeFlash(string $key): void
    {
        $data = $this->allFlash();

        if (isset($data[$key])) {
            unset($data[$key]);
        }

        $this->write('flash', $data);
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function removeAllFlash(): void
    {
        $this->remove('flash');
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function allOldFlash(): array
    {
        $this->handle();
        $data = $this->read('old_flash');

        if (!is_array($data) || $data === null) {
            $data = [];
        }

        return $data;
    }

    /**
     * @param string $key
     * @return mixed
     * @throws \Exception
     */
    public function readOldFlash(string $key)
    {
        $this->handle();

        if (isset($this->read('old_flash')[$key])) {
            return $this->read('old_flash')[$key];
        } else {
            return null;
        }
    }

    /**
     * @param string $key
     * @param mixed $value
     * @throws \Exception
     */
    protected function writeOldFlash(string $key, $value): void
    {
        $data = $this->read('old_flash');

        $flashData = $data;

        $flashData[$key] = $value;

        $this->write('old_flash', $flashData);
    }

    /**
     * @param string $key
     * @throws \Exception
     */
    protected function removeOldFlash(string $key): void
    {
        $data = $this->allOldFlash();

        if (isset($data[$key])) {
            unset($data[$key]);
        }

        $this->write('old_flash', $data);
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function removeAllOldFlash(): void
    {
        $this->remove('old_flash');
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function handleDestruct(): void
    {
        if (!$this->isStarted()) {
            return;
        }
//        $this->handle();
        $flashData = $this->allFlash();
        $oldFlashData = $this->allOldFlash();

        foreach ($oldFlashData as $key => $value) {
            if (!isset($flashData[$key])) {
                $this->removeOldFlash($key);
            }
        }

        foreach ($flashData as $key => $value) {
            if (isset($oldFlashData[$key]) && $oldFlashData[$key] === $value) {
                $this->removeFlash($key);
            } else {
                $this->writeOldFlash($key, $value);
            }
        }
    }

    /**
     * @param string $key
     * @return bool
     */
    protected function handleWriteFlash(string $key): bool
    {
        $file = debug_backtrace()[1]['file'];

        $errorsControllerFile = 'Units/Controllers/ErrorsController.php';

        if ((!endsWith($file, $errorsControllerFile)) && $key === 'validation') {
            return true;
        }

        return false;
    }
}
