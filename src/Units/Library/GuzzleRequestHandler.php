<?php
declare(strict_types=1);

namespace Src\Units\Library;

use Psr\Http\Message\ServerRequestInterface;
use Src\Units\Exceptions\BaseException;
use Src\Units\Library\GuzzleRequest;

/**
 * Class GuzzleRequestHandler
 * @package Src\Units\Library
 */
class GuzzleRequestHandler
{
    /** @var string */
    public $url;
    /** @var array */
    public $headers;
    /** @var bool */
    public $auth;
    /** @var array */
    public $params;
    /** @var GuzzleRequest */
    public $requestGuzzle;
    /**
     * @var Guzzle
     */
    private $guzzle;

    /**
     * GuzzleRequestBuilder constructor.
     * @param ServerRequestInterface $request
     * @param string $endpoint
     * @param array|null $headers
     * @param string|null $authToken
     * @param array $params
     */
    public function __construct(
        ServerRequestInterface $request,
        string $endpoint,
        array $headers = null,
        string $authToken = null,
        array $params = []
    ) {
        $this->guzzle = new Guzzle();

        $url = $request->getUri()->getHost();
        $this->url = $url . ':8000/' . $endpoint;
        $this->params['form']= $params;
        $this->params['query']= $params;
        $this->params['json']= $params;

        if (!is_null($headers))
            $this->headers = $headers;
        if ($headers == null and $authToken != null)
            $this->headers = $this->buildHeader($authToken);
        $this->auth = false;
        if ($authToken)
            $this->auth = true;
    }

    /**
     * @return GuzzleRequest
     */
    public function build(): self {
        $requestGuzzle = new GuzzleRequest(
            $this->url,
            $this->auth,
            $this->headers,
            $this->params
        );
        $this->requestGuzzle = $requestGuzzle;
        return $this;
    }

    /**
     * @return GuzzleRequest
     */
    public function get(): GuzzleRequest {
        return $this->requestGuzzle;
    }

    /**
     * @param $authToken
     * @return array
     */
    public function buildHeader($authToken = null): array
    {
//        $headers['Accept'] = 'application/json';
        if ($authToken)
            $headers['Authorization'] = 'Basic ' . $authToken;
        return $headers;
    }


    /**
     * @param \Src\Units\Library\GuzzleRequest|null $requestGuzzle
     * @param string $method
     * @return array
     * @throws \Exception
     */
    public function makeRequest(string $method = 'GET', GuzzleRequest $requestGuzzle = null): ?array
    {
        if (is_null($requestGuzzle))
            $requestGuzzle = $this->get();
        try {
            $body = $this->guzzle->request($requestGuzzle, $method);
            $body = json_decode($body->getContent(),true);
        } catch (BaseException $e) {
            $body = [];
            if ($e->getCode() ==422)
                $body = json_decode($e->getMessage(),true);
        }
        if ($body['data'])
            return $body['data'];
        return $body;
    }
}
