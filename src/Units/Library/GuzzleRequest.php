<?php
declare(strict_types=1);

namespace Src\Units\Library;

/**
 * Class GuzzleRequest: Responsáveis pelo modelo de Request
 *
 * @package App\Units\Library\Http\Client
 */
class GuzzleRequest
{
    /**
     * @var string
     */
    public $endpoint;

    /**
     * @var array
     */
    public $headers;

    /**
     * @var array
     */
    public $formParams;

    /**
     * @var array
     */
    public $queryParams;

    /**
     * @var array
     */
    public $json;

    /**
     * @var bool
     */
    public $auth;

    /**
     * Request constructor.
     *
     * @since Version 0.1.0
     * @param string $endpoint
     * @param bool   $auth
     * @param array  $headers
     * @param array  $params
     */
    public function __construct($endpoint, $auth = false, $headers = [], $params = [])
    {
        $this->endpoint    = $endpoint;
        $this->auth        = $auth;
        $this->headers     = $headers;
        $this->formParams  = $params['form']  ?? [];
        $this->queryParams = $params['query'] ?? [];
        $this->json        = $params['json'] ?? [];
    }

    /**
     * Faz um merge dos header com um array passado por parâmetro
     *
     * @param array $headers
     */
    public function mergeHeader(array $headers)
    {
        $this->headers = array_merge($this->headers, $headers);
    }

    /**
     * Faz um merge dos formParams com um array passado por parâmetro
     *
     * @param array $formParams
     */
    public function mergeFormParams(array $formParams)
    {
        $this->formParams = array_merge($this->formParams, $formParams);
    }

    /**
     * Faz um merge dos queryParams com um array passado por parâmetro
     *
     * @param array $queryParams
     */
    public function mergeQueryParams(array $queryParams)
    {
        $this->queryParams = array_merge($this->queryParams, $queryParams);
    }

    /**
     * Faz um merge dos params com um array passado por parâmetro
     *
     * @param array $params
     */
    public function mergeParams(array $params)
    {
        $this->formParams  = array_merge($this->formParams,  $params['form']  ?? []);
        $this->queryParams = array_merge($this->queryParams, $params['query'] ?? []);
    }

    /**
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->formParams  = $params['form']  ?? [];
        $this->queryParams = $params['query'] ?? [];
    }

    /**
     * @param string $endpoint
     */
    public function setEndpoint(string $endpoint)
    {
        $this->endpoint = $endpoint;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }
}
