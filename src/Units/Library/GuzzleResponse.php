<?php
declare(strict_types=1);

namespace Src\Units\Library;

//use Illuminate\Http\RedirectResponse as Redirect;
use Psr\Http\Message\ResponseInterface;

/**
 * Class GuzzleResponse: Responsável pelo modelo de Response
 * @package Src\Units\Library
 */
class GuzzleResponse
{
    /**
     * @var String
     */
    public $headers;

    /**
     * @var array | object
     */
    protected $content;

    /**
     * @var string
     */
    protected $version;

    /**
     * @var int
     */
    protected $statusCode;

    /**
     * @var string
     */
    protected $statusText;

//    /**
//     * @var \Illuminate\Http\RedirectResponse
//     */
//    protected $redirect;

    /**
     * @var array
     */
    public static $statusTexts = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',            // RFC2518
        103 => 'Early Hints',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',          // RFC4918
        208 => 'Already Reported',      // RFC5842
        226 => 'IM Used',               // RFC3229
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',    // RFC7238
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Payload Too Large',
        414 => 'URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',                                               // RFC2324
        421 => 'Misdirected Request',                                         // RFC7540
        422 => 'Unprocessable Entity',                                        // RFC4918
        423 => 'Locked',                                                      // RFC4918
        424 => 'Failed Dependency',                                           // RFC4918
        425 => 'Too Early',                                                   // RFC-ietf-httpbis-replay-04
        426 => 'Upgrade Required',                                            // RFC2817
        428 => 'Precondition Required',                                       // RFC6585
        429 => 'Too Many Requests',                                           // RFC6585
        431 => 'Request Header Fields Too Large',                             // RFC6585
        451 => 'Unavailable For Legal Reasons',                               // RFC7725
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',                                     // RFC2295
        507 => 'Insufficient Storage',                                        // RFC4918
        508 => 'Loop Detected',                                               // RFC5842
        510 => 'Not Extended',                                                // RFC2774
        511 => 'Network Authentication Required',                             // RFC6585
    ];

    /**
     * GuzzleResponse constructor.
     *
     * @param string $content
     * @param int $status
     * @param string $version
     * @param array $headers
     */
    public function __construct($content = '', int $status = 200, array $headers = [], $version = '1.0')
    {
        $this->headers = $headers;
        $this->setContent($content);
        $this->setStatusCode($status);
        $this->setProtocolVersion($version);
    }

    /**
     * @param ResponseInterface $interface_response
     * @return GuzzleResponse
     */
    public static function interfaceResponseToResponse(ResponseInterface $interface_response): GuzzleResponse
    {
        $response = new GuzzleResponse(
            $interface_response->getBody(),
            $interface_response->getStatusCode(),
            $interface_response->getHeaders(),
            $interface_response->getProtocolVersion()
        );

        return $response;
    }

    /**
     * Verifica se parâmetro passado pode ser convertido para string e retorna uma exception em caso negativo
     * Caso positivo o parâmetro é salvo no conteúdo da Response
     *
     * @param $content
     * @return GuzzleResponse
     */
    public function setContent($content): GuzzleResponse
    {
        if (null !== $content && !\is_string($content) && !is_numeric($content) && !\is_callable([$content, '__toString'])) {
            throw new \UnexpectedValueException(sprintf('The Response content must be a string or object implementing __toString(), "%s" given.', \gettype($content)));
        }
        $this->content = (string) $content;
        return $this;
    }

    /**
     * Retorna o conteúdo da Response
     *
     * @return String
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Define a versão do protocolo HTTP (1.0 ou 1.1).
     *
     * @param $version String
     * @return $this
     * @final
     */
    public function setProtocolVersion(string $version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * Retorna a versão do protocolo HTTP
     *
     * @return string
     */
    public function getProtocolVersion(): string
    {
        return $this->version;
    }

    /**
     * Define o HTTP statusCode e o HTTP statusTex
     *
     * @param int $code
     * @param string $text
     * @return $this
     * @throws \InvalidArgumentException When the HTTP status code is not valid
     */
    public function setStatusCode(int $code, $text = null)
    {
        $this->statusCode = $code;

        if ($this->isInvalid()) {
            throw new \InvalidArgumentException(sprintf('The HTTP status code "%s" is not valid.', $code));
        }

        if (null === $text) {
            $this->statusText = isset(self::$statusTexts[$code]) ? self::$statusTexts[$code] : 'Unknown status';
            return $this;
        }

        if (false === $text) {
            $this->statusText = '';
            return $this;
        }

        $this->statusText = $text;

        return $this;

    }

    /**
     * Retorna o HTTP statusCode da Response
     *
     * @return int
     * @final
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Retorna o HTTP statusText da Response
     *
     * @return string
     * @final
     */
    public function getStatusText(): string
    {
        return $this->statusText;
    }
//
//    /**
//     * Retorna um objeto Redirect
//     *
//     * @return \Illuminate\Http\RedirectResponse
//     */
//    public function getRedirect(): Redirect
//    {
//        return $this->redirect;
//    }
//
//    /**
//     * Define um objeto Redirect
//     *
//     * @param \Illuminate\Http\RedirectResponse
//     */
//    public function setRedirect(Redirect $redirect)
//    {
//        $this->redirect = $redirect;
//    }
//
    /**
     * A Response tem um status de inválido?
     *
     * @see http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
     * @final
     */
    public function isInvalid(): bool
    {
        return $this->statusCode < 100 || $this->statusCode >= 600;
    }

    /**
     * A Response tem um status informativo?
     *
     * @final
     */
    public function isInformational(): bool
    {
        return $this->statusCode >= 100 && $this->statusCode < 200;
    }

    /**
     * A Response tem um status de sucesso?
     *
     * @final
     */
    public function isSuccessful(): bool
    {
        return $this->statusCode >= 200 && $this->statusCode < 300;
    }

    /**
     * A Response é um redirecionamento?
     *
     * @final
     */
    public function isRedirection(): bool
    {
        return $this->statusCode >= 300 && $this->statusCode < 400;
    }

    /**
     * A Response é um erro de Client?
     *
     * @final
     */
    public function isClientError(): bool
    {
        return $this->statusCode >= 400 && $this->statusCode < 500;
    }

    /**
     * A Response é um erro de Servidor?
     *
     * @final
     */
    public function isServerError(): bool
    {
        return $this->statusCode >= 500 && $this->statusCode < 600;
    }

    /**
     * A Response é um status OK?
     *
     * @final
     */
    public function isOk(): bool
    {
        return 200 === $this->statusCode;
    }

    /**
     * A Response é um status Created?
     *
     * @final
     */
    public function isCreated(): bool
    {
        return 201 === $this->statusCode;
    }

    /**
     * A Response é um status de não autorizado?
     *
     * @final
     */
    public function isUnauthorized(): bool
    {
        return 401 === $this->statusCode;
    }

    /**
     * A Response é um status de acesso proibido?
     *
     * @final
     */
    public function isForbidden(): bool
    {
        return 403 === $this->statusCode;
    }

    /**
     * A Response é um status de não encontrado?
     *
     * @final
     */
    public function isNotFound(): bool
    {
        return 404 === $this->statusCode;
    }

    /**
     * A Response é um status de erro de método não habilitado?
     *
     * @final
     */
    public function isMethodNotAllowed(): bool
    {
        return 405 === $this->statusCode;
    }

    /**
     * A Response é um status de entidade não processável?
     *
     * @final
     */
    public function isUnprocessableEntity(): bool
    {
        return 422 === $this->statusCode;
    }

}
