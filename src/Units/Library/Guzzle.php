<?php
declare(strict_types=1);

namespace Src\Units\Library;

use Src\Units\Library\GuzzleRequest as Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Src\Units\Exceptions\BaseException;

/**
 * Class GuzzleClient: Responsáveis pelas requisições HTTP
 * @package Src\Units\Library
 */
class Guzzle
{
    /**
     * @var Client
     */
    private $guzzle;

    /**
     * HttpHelper constructor.
     *
     * @param string $url
     */
    public function __construct(string $url = '//127.0.0.1')
    {
        $this->guzzle  = new Client(['base_uri' => $url]);
    }

    /**
     * Faz um chamada via verbo GET para a API
     *
     * @param GuzzleRequest $request
     * @return GuzzleResponse
     * @throws \Exception
     */
    public function get(Request $request): GuzzleResponse
    {
        return $this->request($request, 'GET');
    }
    /**
     * Faz um chamada via verbo POST para a API
     *
     * @param GuzzleRequest $request
     * @return GuzzleResponse
     * @throws \Exception
     */
    public function post(Request $request): GuzzleResponse
    {
        return $this->request($request, 'POST');
    }
    /**
     * Faz um chamada via verbo PUT para a API
     *
     * @param GuzzleRequest $request
     * @return GuzzleResponse
     * @throws \Exception
     */
    public function put(Request $request): GuzzleResponse
    {
        return $this->request($request, 'PUT');
    }
    /**
     * Faz um chamada via verbo PATCH para a API
     *
     * @param GuzzleRequest $request
     * @return GuzzleResponse
     * @throws \Exception
     */
    public function patch(Request $request): GuzzleResponse
    {
        return $this->request($request, 'PATCH');
    }

    /**
     * Faz um chamada via verbo DELETE para a API
     *
     * @param GuzzleRequest $request
     * @return GuzzleResponse
     * @throws \Exception
     */
    public function delete(Request $request): GuzzleResponse
    {
        return $this->request($request, 'DELETE');
    }

    /**
     * Faz um chamada para a API de acordo com o parâmetro passado como parâmetro
     *
     * @param string $method
     * @param GuzzleRequest $request
     * @return GuzzleResponse
     * @throws \Exception
     */
    public function request(Request $request, $method = 'GET'): GuzzleResponse
    {
        $endpoint = $this->cleanEndpoint($request->endpoint);
//        $request->headers['APP-LOCALE'] = app('translator')->getLocale() ?? 'pt-BR';
        $options = [
            'headers'     => $request->headers,
            'form_params' => $request->formParams,
//            'query'       => $request->queryParams,
//            'json'       => $request->json
        ];
//        dd($options);
        try {

            $response = $this->makeRequest($endpoint, $method, $options);

        } catch (ClientException | ServerException | \Exception $ex) {

            $guzzle_response = $ex->getResponse();
            $response = GuzzleResponse::interfaceResponseToResponse($guzzle_response);
            $content = json_decode($response->getContent(), true);
            $message = $content['reason_phrase'];
            if (!$message)
                $message = 'Unknown API error';
            if ($response->getStatusCode() === 422) {
                throw new \Src\Units\Exceptions\ValidationException($response->getContent(),422);
//                return $content;
//                $validatorResponse = new Response(json_encode($message), $response->getStatusCode(), $response->headers);
//                throw new ValidationException('', $validatorResponse, $validatorResponse);
            } elseif ($response->getStatusCode() >= 400 && $response->getStatusCode() <= 499)
                throw new BaseException($message, $response->getStatusCode());
            else
                throw new \Exception($message);
        }

        return $response;
    }

    /**
     * @param string $endpoint
     * @param string $method
     * @param array $options
     * @return GuzzleResponse
     * @throws BaseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function makeRequest(string $endpoint, string $method, array $options): GuzzleResponse
    {
        $methods = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'];

        switch ($method) {
            case 'GET':    $guzzle_response = $this->guzzle->get   ($endpoint, $options); break;
            case 'POST':   $guzzle_response = $this->guzzle->post  ($endpoint, $options); break;
            case 'PUT':    $guzzle_response = $this->guzzle->put   ($endpoint, $options); break;
            case 'PATCH':  $guzzle_response = $this->guzzle->patch ($endpoint, $options); break;
            case 'DELETE': $guzzle_response = $this->guzzle->delete($endpoint, $options); break;
            default: throw new BaseException($methods, '');
        }

        return GuzzleResponse::interfaceResponseToResponse($guzzle_response);
    }

    /**
     * Remove espaços extras antes e depois cada caracter '/' (barra)
     *
     * @param $endpoint
     * @return string
     */
    private function cleanEndpoint($endpoint) {
        $endpoint = ltrim($endpoint, "/");
        $endpoint = rtrim($endpoint, "/");
        return $endpoint;
    }
}
