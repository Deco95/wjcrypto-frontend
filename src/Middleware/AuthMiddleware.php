<?php
declare(strict_types=1);

namespace Src\Middleware;

use Laminas\Diactoros\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Laminas\Diactoros\Response\RedirectResponse;
use Src\Application\User\UserService;
use Src\Units\Handlers\SessionHandler;

/**
 * Middleware Interface responsible for implementing user authentication
 *
 * Class AuthMiddleware
 * @package Src\Middleware
 */
class AuthMiddleware implements MiddlewareInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws \Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $auth = false;
        $userToken = $this->getValidFlashData('authToken');

        if ($userToken['authToken'])
            $auth = true;
        if ($auth === true) {
//            session()->writeFlash('authToken',$user['authToken']);
            return $handler->handle($request);
        }

        $uri = $request->getUri();
        return new RedirectResponse(
            $uri->withPath('')
        );
    }

    /**
     * @param array $data
     * @param string $name
     * @return array
     * @throws \Exception
     */
    protected function getValidFlashData(string $name, array $data = []): array
    {
        $flashData = session(true)->readFlash($name);

        if ($flashData !== null && ! empty($flashData)) {
            $data[$name] = $flashData;
        }

        return $data;
    }

    /**
     * @throws \Exception
     */
    public function __destruct()
    {
        $sessionLogout = $this->getValidFlashData('logout');
        if ($sessionLogout)
            session()->handleDestruct();
    }
}
