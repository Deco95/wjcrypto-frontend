<?php
declare(strict_types=1);

namespace Src\Application\Transaction;

use Src\Infrastructure\Repository\RepositoryInterface;

/**
 * Interface TransactionRepositoryInterface
 * @package Src\Application\Transaction
 */
interface TransactionRepositoryInterface extends RepositoryInterface
{
    /**
     * TransactionRepositoryInterface constructor.
     * @param TransactionModel $model
     */
    public function __construct(TransactionModel $model);

}
