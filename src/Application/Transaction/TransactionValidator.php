<?php
declare(strict_types=1);

namespace Src\Application\Transaction;

use Src\Infrastructure\Validator\BaseValidator;
use Src\Units\Exceptions\BaseException;
use Src\Units\Exceptions\ValidationException;
use Src\Units\FieldValidator;

/**
 * Class CategoryValidator
 * @package Src\Validator
 */
class TransactionValidator extends BaseValidator
{
    /**
     * CategoryValidator constructor.
     * @param string|null $mode
     * @param int|null    $modelId
     */
    public function __construct(?string $mode, ?int $modelId = null)
    {
        parent::__construct($mode, $modelId);
        $this->model = new TransactionModel();
    }

    /**
     * @return array
     */
    public function getCreateValidator(): array
    {
        return [
            'transaction_type' => new FieldValidator(false, true,  'string,integer', false),
            'account_debit' => new FieldValidator(false, false,  'integer', false),
            'account_credit' => new FieldValidator(false, false,  'integer', false),
            'debit' => new FieldValidator(false, false,  'integer', false),
            'credit' => new FieldValidator(false, false,  'integer', false),
        ];
    }

    /**
     * @return array
     */
    public function getUpdateValidator(): array
    {
        return [
            'transaction_type' => new FieldValidator(false, false,  'string,integer', false),
            'account_debit' => new FieldValidator(false, false,  'integer', false),
            'account_credit' => new FieldValidator(false, false,  'integer', false),
            'debit' => new FieldValidator(false, false,  'integer', false),
            'credit' => new FieldValidator(false, false,  'integer', false),
        ];
    }

    /**
     * @param $value
     * @param bool $hasCurrency
     * @throws ValidationException
     */
    public function validateTransactionValue($value, bool $hasCurrency = true): void
    {
        if (!isset($value))
            throw new BaseException('Transaction value must be integer',422);
        if (!$hasCurrency)
            throw new BaseException('Transaction value exceeds user\'s Balance',422);
    }
}
