<?php
declare(strict_types=1);

namespace Src\Application\Transaction;

use Laminas\Diactoros\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Src\Infrastructure\Controllers\BaseController;

/**
 * Class CurrencyController
 * @package Src\Application\Currency
 */
class TransactionController extends BaseController
{
    /** @var string */
    protected $serviceClass = TransactionService::class;

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getTransactions(ServerRequestInterface $request) : ResponseInterface
    {
        $body = $request->getParsedBody();
        $user = $this->newService()->getTransactions();
        return $this->buildResponse($user, 200);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function debit(ServerRequestInterface $request) : ResponseInterface
    {
        $body = $request->getParsedBody();
        $user = $this->newService()->debit($body);
        return $this->buildResponse($user, 200);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function credit(ServerRequestInterface $request) : ResponseInterface
    {
        $body = $request->getParsedBody();
        $user = $this->newService()->credit($body);
        return $this->buildResponse($user, 200);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function transfer(ServerRequestInterface $request) : ResponseInterface
    {
        $body = $request->getParsedBody();
        $user = $this->newService()->transfer($body);
        return $this->buildResponse($user, 200);
    }
}
