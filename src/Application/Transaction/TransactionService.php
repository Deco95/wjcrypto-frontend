<?php
declare(strict_types=1);

namespace Src\Application\Transaction;

use Src\Application\CurrencyBalance\CurrencyBalanceService;
use Src\Application\User\UserService;
use Src\Infrastructure\Models\BaseModel;
use Src\Infrastructure\Services\BaseService;
use Src\Infrastructure\Validator\ValidatorInterface;
use Src\Units\Exceptions\BaseException;
use Src\Units\Exceptions\ValidationException;

/**
 * Class TransactionService
 * @package Src\Application\Transaction
 */
class TransactionService extends BaseService
{
    /** @var */
    private $currencyBalanceService = CurrencyBalanceService::class;
    /** @var */
    private $userService = UserService::class;

    /**
     * TransactionService constructor.
     * @param BaseModel $modelClass
     * @param ValidatorInterface $validatorClass
     * @param TransactionRepositoryInterface $repositoryClass
     */
    public function __construct(
        BaseModel $modelClass = null,
        ValidatorInterface $validatorClass = null,
        TransactionRepositoryInterface $repositoryClass = null
    )
    {
        $this->modelClass = $modelClass;
        $this->validatorClass = $validatorClass;
        $this->repositoryClass = $repositoryClass;
        if ($modelClass === null)
            $this->modelClass = TransactionModel::class;
        if ($validatorClass === null)
            $this->validatorClass = TransactionValidator::class;
        if ($repositoryClass === null)
            $this->repositoryClass = TransactionRepository::class;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getTransactions(): array
    {
        $sessionAccount = $this->getUserAccount();
        $body['credit'] = $this->getAccountDebit($sessionAccount);
        $body['debit'] = $this->getAccountCredit($sessionAccount);
        $body['balance'] = $body['credit'] - $body['debit'];

        return $body;
    }

    /**
     * @param array $body
     * @return array
     * @throws ValidationException
     * @throws \Exception
     */
    public function debit(array $body): array
    {
        $sessionAccount = $this->getUserAccount();
        $transaction = $this->transactionBuilder($body,'debit');

        $this->validate($this->validatorClass::CREATE_MODE, $transaction);
        $this->validateDebit($transaction['debit'], $sessionAccount);
        $item = $this->create($transaction);
        $this->newCurrencyBalanceService()->updateBalance();
        return $item;
    }

    /**
     * @param array $body
     * @return array
     * @throws ValidationException
     * @throws \Exception
     */
    public function credit(array $body): array
    {
        $transaction = $this->transactionBuilder($body,'credit');

        $this->validate($this->validatorClass::CREATE_MODE, $transaction);
        $this->validateCredit($transaction['credit']);

        $item = $this->create($transaction);
        $this->newCurrencyBalanceService()->updateBalance();
        return $item;
    }

    /**
     * @param array $body
     * @return array
     * @throws ValidationException
     * @throws \Exception
     */
    public function transfer(array $body): array
    {
        $sessionAccount = $this->getUserAccount();
        $body['account_credit'] = $this->newUserService()->getUserAccountByTaxVat($body['taxvat']);
        $transaction = $this->transactionBuilder($body,'transfer');

        $this->validate($this->validatorClass::CREATE_MODE, $transaction);
        $this->validateCredit($transaction['credit']);
        $this->validateDebit($transaction['debit'], $sessionAccount);

        $item = $this->create($transaction);
        $this->newCurrencyBalanceService()->updateBalance();
        $this->newCurrencyBalanceService()->updateBalance($transaction['account_credit']);
        return $item;
    }

    /**
     * @param array $body
     * @param string $type
     * @return array
     * @throws \Exception
     */
    public function transactionBuilder(array $body, string $type): array
    {
        $sessionAccount = $this->getUserAccount();
        switch ($type){
            case 'debit':
                $transaction['debit'] = $body['value'];
                $transaction['account_debit'] = $sessionAccount;
                break;
            case 'credit':
                $transaction['credit'] = $body['value'];
                $transaction['account_credit'] = $sessionAccount;
                break;
            case 'transfer':
                $transaction['debit'] = $body['value'];
                $transaction['account_debit'] = $sessionAccount;
                $transaction['credit'] = $body['value'];
                $transaction['account_credit'] = $body['account_credit'];
                break;
            default:
                throw new BaseException('Transaction not found:' . $type);
                break;
        }
        $transaction['transaction_type'] = $type;

        return $transaction;
    }

    /**
     * @param $value
     * @param int $UserAccount
     */
    public function validateDebit($value, int $UserAccount): void
    {
        $currencyService = $this->newCurrencyBalanceService();
        $hasCurrency = $currencyService->hasCurrency($value, $UserAccount);

        $this->newValidator($this->validatorClass::CREATE_MODE)
            ->validateTransactionValue($value,$hasCurrency);
    }

    /**
     * @param $value
     */
    public function validateCredit($value): void
    {
        $this->newValidator($this->validatorClass::CREATE_MODE)
            ->validateTransactionValue($value);
    }

    /**
     * @return CurrencyBalanceService
     */
    protected function newCurrencyBalanceService(): CurrencyBalanceService
    {
        return new $this->currencyBalanceService;
    }

    /**
     * @return UserService
     */
    protected function newUserService(): UserService
    {
        return new $this->userService;
    }

    /**
     * @param array $body
     * @return array
     */
    private function create(array $body): array
    {
        $entity = $this->newRepository()->create($body);
        return $entity->toArray();
    }

    /**
     * @param int $id
     * @return bool
     */
    public function exists(int $id): bool
    {
        try {
            $this->newRepository()->getById($id);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * @param int $userAccount
     * @return int
     */
    public function getAccountCredit(int $userAccount): int
    {
        return $this->newRepository()->getAccountCredit($userAccount);
    }

    /**
     * @param int $userAccount
     * @return int
     */
    public function getAccountDebit(int $userAccount): int
    {
        return $this->newRepository()->getAccountDebit($userAccount);
    }
}
