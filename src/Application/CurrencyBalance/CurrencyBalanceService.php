<?php
declare(strict_types=1);

namespace Src\Application\CurrencyBalance;

use Src\Application\Transaction\TransactionService;
use Src\Infrastructure\Models\BaseModel;
use Src\Infrastructure\Services\BaseService;
use Src\Infrastructure\Validator\ValidatorInterface;
use Src\Units\Exceptions\BaseException;
use Src\Units\Exceptions\ValidationException;

/**
 * Class CurrencyService
 * @package Src\Application\Currency
 */
class CurrencyBalanceService extends BaseService
{
    /** @var string */
    protected $transactionService = TransactionService::class;

    /**
     * CurrencyBalanceService constructor.
     * @param BaseModel $modelClass
     * @param ValidatorInterface $validatorClass
     * @param CurrencyBalanceRepositoryInterface|null $repositoryClass
     */
    public function __construct(
        BaseModel $modelClass = null,
        ValidatorInterface $validatorClass = null,
        CurrencyBalanceRepositoryInterface $repositoryClass = null
    )
    {
        $this->modelClass = $modelClass;
        $this->validatorClass = $validatorClass;
        $this->repositoryClass = $repositoryClass;
        if ($modelClass === null)
            $this->modelClass = CurrencyBalanceModel::class;
        if ($validatorClass === null)
            $this->validatorClass = CurrencyBalanceValidator::class;
        if ($repositoryClass === null)
            $this->repositoryClass = CurrencyBalanceRepository::class;
    }

    /**
     * @param int $userAccount
     * @return void
     * @throws BaseException
     * @throws ValidationException
     * @throws \Exception
     */
    public function newCurrencyBalance(int $userAccount): void
    {
        if (!$userAccount)
            $userAccount = $this->getUserAccount();
        $body[$this->modelClass::ACCOUNT_NUMBER] = $userAccount;
        $exists = $this->newRepository()->accountExists($userAccount);
        $body[$this->modelClass::ACCOUNT_BALANCE] = 0;
        $body[$this->modelClass::TOTAL_ACCOUNT_DEBIT] = 0;
        $body[$this->modelClass::TOTAL_ACCOUNT_CREDIT] = 0;
        if (!$exists)
            $this->create($body);
    }

    /**
     * @param int $userAccount
     * @return int
     * @throws \Exception
     */
    public function updateBalance(int $userAccount = null): int
    {
        if (!$userAccount)
            $userAccount = $this->getUserAccount();
        $body[$this->modelClass::ACCOUNT_NUMBER] = $userAccount;
        $this->newRepository()->accountExistsOrFail($userAccount);

        $totalCredit = $this->newTransactionService()->getAccountCredit($userAccount);
        $totalDebit = $this->newTransactionService()->getAccountDebit($userAccount);
        $balance = $totalCredit - $totalDebit;

        return $this->newRepository()->setBalance($userAccount,$balance);
    }

    /**
     * @return TransactionService
     */
    protected function newTransactionService(): TransactionService
    {
        return new $this->transactionService;
    }

    /**
     * @param array $body
     * @return array
     * @throws ValidationException
     */
    private function create(array $body): array
    {
        $this->validate($this->validatorClass::CREATE_MODE, $body);
        $entity = $this->newRepository()->create($body);
        return $entity->toArray();
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function getBalance(): int
    {
        $userAccount = $this->getUserAccount();
        $this->updateBalance($userAccount);
        return $this->newRepository()->getBalance($userAccount);
    }

    /**
     * @param int $value
     * @param int $UserAccount
     * @return bool
     */
    public function hasCurrency($value, int $UserAccount): bool
    {
        return $this->newRepository()->hasCurrency($value, $UserAccount);
    }
}
