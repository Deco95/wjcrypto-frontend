<?php
declare(strict_types=1);

namespace Src\Application\User;

use Src\Infrastructure\Repository\RepositoryInterface;

/**
 * Interface UserRepositoryInterface
 * @package Src\Application\User
 */
interface UserRepositoryInterface extends RepositoryInterface
{
    /**
     * UserRepositoryInterface constructor.
     * @param UserModel $model
     */
    public function __construct(UserModel $model);

}
