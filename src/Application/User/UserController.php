<?php
declare(strict_types=1);

namespace Src\Application\User;

use Laminas\Diactoros\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Src\Infrastructure\Controllers\BaseController;
use Src\Units\Library\GuzzleRequestHandler;
use Src\Units\Library\Logs\CustomLogger;
use Monolog\Logger;

/**
 * Class CurrencyController
 * @package Src\Application\Currency
 */
class UserController extends BaseController
{
    /** @var Logger */
    protected $logger;

    public function __construct(ResponseInterface $response = null)
    {
        parent::__construct($response);
        $logger = new CustomLogger('WJCrypto.UserController');
        $this->logger = $logger;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function home(ServerRequestInterface $request): ResponseInterface
    {
        $this->logger->info('home',[$request->getParsedBody()],[$request->getHeaders()]);

        session(true)->handleDestruct();

        $response = blade()->run('login', array("title" => "WJCrypto", "body" => $body['data']));
        return $this->buildResponse($response);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function login(ServerRequestInterface $request): ResponseInterface
    {
        session(true)->handleDestruct();
        $body = $request->getParsedBody($request);
        $email = $body['email'];
        $pass = $body['password'];

        $basicAuthToken = $this->buildBasicAuthToken($email, $pass);
        $body = $this->getUserData($request,$basicAuthToken);
//        $body = $this->newService()->getUserData($request,$basicAuthToken);

        if ($body){
            session(true)->writeFlash('authToken',$basicAuthToken);
        }

        $response = blade()->run('home', array("title" => "WJCrypto", "body" => $body));
        return $this->buildResponse($response);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function editUser(ServerRequestInterface $request): ResponseInterface
    {
        $basicAuthToken = session(true)->readFlash('authToken');
        $body = $this->getUserData($request,$basicAuthToken);

        $response = blade()->run('editUser', array("title" => "WJCrypto", "body" => $body));
        return $this->buildResponse($response);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function addUser(ServerRequestInterface $request): ResponseInterface
    {
        $response = blade()->run('addUser', array("title" => "WJCrypto"));
        return $this->buildResponse($response);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function dashboard(ServerRequestInterface $request): ResponseInterface
    {
        $basicAuthToken = session(true)->readFlash('authToken');
        $body = $this->getUserData($request,$basicAuthToken);
        $balance = $this->getUserBalance($request,$basicAuthToken);
        $response = blade()->run('home', array("title" => "WJCrypto", "body" => $body, "balance" => $balance['balance']));
        return $this->buildResponse($response);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function credit(ServerRequestInterface $request): ResponseInterface
    {
        $basicAuthToken = session(true)->readFlash('authToken');
        $body = $this->getUserData($request,$basicAuthToken);
        $transaction = $this->makeTransaction(
            $request,$basicAuthToken,
            'transaction/credit',
            $request->getParsedBody()
        );
        $transaction['name'] = $body['name'];
        $balance = $this->getUserBalance($request,$basicAuthToken);

        $response = blade()->run('home', array("title" => "WJCrypto", "body" => $transaction, "balance" => $balance['balance']));
        return $this->buildResponse($response);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function debit(ServerRequestInterface $request): ResponseInterface
    {
        $basicAuthToken = session(true)->readFlash('authToken');
        $body = $this->getUserData($request,$basicAuthToken);
        $transaction = $this->makeTransaction(
            $request,$basicAuthToken,
            'transaction/debit',
            $request->getParsedBody()
        );
        $transaction['name'] = $body['name'];
        $balance = $this->getUserBalance($request,$basicAuthToken);

        $response = blade()->run('home', array("title" => "WJCrypto", "body" => $transaction, "balance" => $balance['balance']));
        return $this->buildResponse($response);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function transfer(ServerRequestInterface $request): ResponseInterface
    {
        $basicAuthToken = session(true)->readFlash('authToken');
        $body = $this->getUserData($request,$basicAuthToken);
        $transaction = $this->makeTransaction(
            $request,$basicAuthToken,
            'transaction/transfer',
            $request->getParsedBody()
        );
        $transaction['name'] = $body['name'];
        $balance = $this->getUserBalance($request,$basicAuthToken);

        $response = blade()->run('home', array("title" => "WJCrypto", "body" => $transaction, "balance" => $balance['balance']));
        return $this->buildResponse($response);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function register(ServerRequestInterface $request): ResponseInterface
    {
        session(true)->handleDestruct();
        $body = $request->getParsedBody();
        $body = $this->createUserData($request,$body);

        if ($body['status_code']){
            $response = blade()->run('addUser', array("title" => "WJCrypto", "body" => $body));
            return $this->buildResponse($response);
        }
        $response = blade()->run('login', array("title" => "WJCrypto", "body" => $body));
        return $this->buildResponse($response);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function updateUser(ServerRequestInterface $request): ResponseInterface
    {
        $basicAuthToken = session(true)->readFlash('authToken');
        $body = $request->getParsedBody();
        $body = $this->updateUserData($request,$basicAuthToken,$body);

        if ($body['status_code']){
            $response = blade()->run('editUser', array("title" => "WJCrypto", "body" => $body));
            return $this->buildResponse($response);
        }
        $response = blade()->run('login', array("title" => "WJCrypto", "body" => $body));
        return $this->buildResponse($response);
    }

    /**
     * @param ServerRequestInterface $request
     * @param string $authToken
     * @param array|null $params
     * @return array
     * @throws \Exception
     */
    public function makeTransaction(ServerRequestInterface $request, string $authToken, $endpoint, array $params = []): array
    {
        $requestGuzzle = new GuzzleRequestHandler($request,$endpoint,null,$authToken,$params);

        $body = $requestGuzzle->build()->makeRequest('POST');

        if ($body['data'])
            return $body['data'];
        return $body;
    }

    /**
     * @param ServerRequestInterface $request
     * @param string $authToken
     * @param array|null $params
     * @return array
     * @throws \Exception
     */
    public function getUserBalance(ServerRequestInterface $request, string $authToken, array $params = []): array
    {
        $endpoint = 'currency/balance';
        $requestGuzzle = new GuzzleRequestHandler($request,$endpoint,null,$authToken,$params);

        $body = $requestGuzzle->build()->makeRequest('GET');

        if ($body['data'])
            return $body['data'];
        return $body;
    }

    /**
     * @param ServerRequestInterface $request
     * @param string $authToken
     * @param array|null $params
     * @return array
     * @throws \Exception
     */
    public function getUserData(ServerRequestInterface $request, string $authToken, array $params = []): array
    {
        $endpoint = 'user';
        $requestGuzzle = new GuzzleRequestHandler($request,$endpoint,null,$authToken,$params);
        $body = $requestGuzzle->build()->makeRequest('GET');

        if ($body['data'])
            return $body['data'];
        return $body;
    }

    /**
     * @param ServerRequestInterface $request
     * @param string $authToken
     * @param array|null $params
     * @return array
     */
    public function updateUserData(ServerRequestInterface $request, string $authToken, array $params = []): array
    {
        $endpoint = 'user/update';
        $requestGuzzle = new GuzzleRequestHandler($request,$endpoint,null,$authToken,$params);
        $body = $requestGuzzle->build()->makeRequest('POST');

        if ($body['data'])
            return $body['data'];
        return $body;
    }

    /**
     * @param ServerRequestInterface $request
     * @param string $authToken
     * @param array|null $params
     * @return array
     */
    public function createUserData(ServerRequestInterface $request, array $params = []): array
    {
        $endpoint = 'register';
        $requestGuzzle = new GuzzleRequestHandler($request,$endpoint,null,null,$params);
        $body = $requestGuzzle->build()->makeRequest('POST');

        if ($body['data'])
            return $body['data'];
        return $body;
    }

    /**
     * @param $email
     * @param int $pass
     * @return ResponseInterface
     */
    public function buildBasicAuthToken($email, $pass = 200): string
    {
        return base64_encode($email . ':' . $pass);
    }

    /**
     * @param $data
     * @param int $code
     * @return ResponseInterface
     * @throws \Exception
     */
    public function buildResponse($data, int $code = 200): ResponseInterface
    {
        if ($data instanceof ResponseInterface)
            return $data->withStatus($code);
        $this->response->getBody()->write($data);
        return $this->response->withStatus($code);
    }
}
