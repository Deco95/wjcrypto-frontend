<?php
declare(strict_types=1);

namespace Src\Infrastructure\Repository;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Src\Units\Exceptions\BaseException;

/**
 * Class EloquentRepository
 * @package Src\Infrastructure\Repository
 */
abstract class EloquentRepository extends BaseRepository  implements RepositoryInterface
{
    /**
     * Nome da classe da Model
     *
     * @var string
     */
    protected $modelClass = Model::class;

    /**
     * Cria uma instância do queryBuilder do Laravel
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function newQuery(): Builder
    {
//        return app()->make($this->modelClass)->newQuery();
        return (new $this->modelClass)->newQuery();
    }

    /**
     * Faz um (select * from table) no banco.
     *
     * Se o parâmetro $builder não é nulo, é feito uma nova instância do queryBuilder.
     * Se o parâmetro $paginate é falso ele carrega os dados sem paginação, se é verdadeiro os dados são paginados.
     * Se o parâmetro $take é falso, todos os registros do banco são carregados, caso contrário são carregados o
     *   número de registros passados em $take.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $take
     * @param bool $paginate
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator | \Illuminate\Database\Query\Builder[] | \Illuminate\Support\Collection
     */
    protected function doQuery(Builder $query = null, int $take = 15, bool $paginate = true, array $params = [])
    {
        if (!$query) $query = $this->newQuery();

        if ($paginate) return $query->paginate($take);

        if ($take) return $query->take($take)->where($params)->get();

        return $query->where($params)->get();
    }

    /**
     * {@inheritdoc}
     */
    public function factory(array $data = []): Model
    {
        $model = $this->newQuery()->getModel();

        $this->fillModel($model, $data);

        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function getAll(int $take = 15, bool $paginate = true, array $params = [])
    {
        return $this->doQuery(null, $take, $paginate, $params);
    }

    /**
     * {@inheritdoc}
     */
    public function getById(int $id): Model
    {
        $query = $this->newQuery();

        return $query->findOrFail($id);
    }

    /**
     * Salva no banco o Model passado como parâmetro como um novo registro.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function save(Model $model): Model
    {
        $model->save();

        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data = []): Model
    {
        $model = $this->factory($data);

        return $this->save($model);
    }

    /**
     * {@inheritdoc}
     */
    public function update(int $id, array $data = []): Model
    {
        $model = $this->getById($id);
        $this->fillModel($model, $data);

        return $this->save($model);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(int $id): Model
    {
        $model = $this->getById($id);
        $model->delete();
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteCollection(Collection $collection): Collection
    {
        $collection->each( function(Model $model) {
            $this->delete($model->id);
        });

        return $collection;
    }

    /**
     * {@inheritdoc}
     * @throws BaseException
     */
    public function restore(int $id): Model
    {
        $query = $this->newQuery();

        $restore = $query->withTrashed()
            ->findOrFail($id)->restore();

        if (!$restore) throw new BaseException('Not Found');

        return $this->getById($id);
    }

    /**
     * {@inheritdoc}
     */
    public function updateOrCreate(array $attributes, array $data = array()): Model
    {
        $query = $this->newQuery();
        $model = $query->firstOrNew($attributes);
        $this->fillModel($model, $data)->save();

        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function firstOrFail(array $attributes): Model
    {
        $query = $this->newQuery();
        return $query->firstOrFail($attributes);
    }

    /**
     * Popula a model com o array, ambos são passados por parâmetro.
     *
     * @param Model $model
     * @param array $data
     * @return Model
     */
    public function fillModel(Model $model, array $data = []): Model
    {
        $model->fill($data);

        return $model;
    }
}
