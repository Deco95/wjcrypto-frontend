<?php
declare(strict_types=1);

namespace Src\Infrastructure\Controllers;

use Monolog\Logger;
use phpDocumentor\Reflection\Types\Self_;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Laminas\Diactoros\Response;
use Src\Infrastructure\Services\BaseService;
use Src\Units\Library\Logs\CustomLogger;

/** Default Controller for return json data with Flash data from session.
 *
 * Class BaseController
 * @package Src\Infrastructure\Controllers
 */
class BaseController
{
    /** @var string */
    protected $serviceClass = BaseService::class;

    /** @var Response */
    protected $response;

    public function __construct(ResponseInterface $response = null)
    {
        $this->response = $response;
        if ($response === null)
            $this->response = new Response;
    }

    /**
     * @return BaseService
     */
    protected function newService(): BaseService
    {
        return new $this->serviceClass;
    }

    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    protected function getSessionToken(array $data): array
    {
        $data = $this->getValidFlashData($data, 'authToken');
        return $data;
    }

    /**
     * @param array $data
     * @param string $name
     * @return array
     * @throws \Exception
     */
    protected function getValidFlashData(array $data, string $name): array
    {
        $flashData = session()->readFlash($name);

        if ($flashData !== null && ! empty($flashData)) {
            $data[$name] = $flashData;
        }

        return $data;
    }
}
