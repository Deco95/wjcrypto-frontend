<?php
/**
 * This file contains all the routes for the project
 */

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use League\Route\Strategy\ApplicationStrategy;
use Src\Application\User\UserController;

$responseFactory = new \Laminas\Diactoros\ResponseFactory();
$strategy = new Src\Units\Strategy\CustomStrategy($responseFactory);
$router   = (new League\Route\Router)->setStrategy($strategy);
$router->middleware(new Src\Middleware\CustomExceptionMiddleware);

// map a route
$router->map('GET', '/foo', function (ServerRequestInterface $request) : ResponseInterface {
    $response = new Laminas\Diactoros\Response;
    $response->getBody()->write('<h1>Hello, World!</h1>');
    $response->getBody()->write('<h1>Hello, World!</h1>');
    return $response;
});
// User Routes
$router->map('GET', '/',[UserController::class, 'home']);

$router->map('GET', '/register',[UserController::class, 'addUser']);
$router->map('POST', '/user/register',[UserController::class, 'register']);
$router->map('POST', '/user/login',[UserController::class, 'login']);

$router->group('/user', function (\League\Route\RouteGroup $route) {
    $route->map('GET', '/home',[UserController::class, 'dashboard']);
    $route->map('POST', '/credit',[UserController::class, 'credit']);
    $route->map('POST', '/debit',[UserController::class, 'debit']);
    $route->map('POST', '/transfer',[UserController::class, 'transfer']);
    $route->map('GET', '/',[UserController::class, 'editUser']);
    $route->map('POST', '/',[UserController::class, 'updateUser']);
})->middleware(new Src\Middleware\AuthMiddleware);
