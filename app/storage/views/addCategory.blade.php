@extends('template')
@section('content')
@php
$category = $data;
@endphp
<!-- Main Content -->
<main class="content">
    <h1 class="title new-item">New Category</h1>

    <form action="http://{{url()->getHost()}}/api/categories" method="post">
        @method('POST')
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="input-field">
            <label for="category-name"  class="label">Category Name</label>
            <input type="text" id="category-name" name="name" class="input-text" />

        </div>
        <div class="input-field">
            <label for="category-code" class="label">Category Code</label>
            <input type="text" id="category-code"  name="code"  class="input-text" />

        </div>
        <div class="actions-form">
            <a href="http://{{url()->getHost()}}/categories" class="action back">Back</a>
            <input class="btn-submit btn-action"  type="submit" value="Save" />
        </div>
    </form>
</main>
<!-- Main Content -->
@stop
