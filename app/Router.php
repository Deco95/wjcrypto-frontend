<?php
/**
 * Custom router which handles default middlewares, default exceptions and things
 * that should be happen before and after the router is initialised.
 */
namespace App;

use Pecee\SimpleRouter\SimpleRouter;

/**
 * Class Router
 * @package App
 */
class Router extends SimpleRouter
{
    /**
     * @throws \Pecee\Http\Middleware\Exceptions\TokenMismatchException
     * @throws \Pecee\SimpleRouter\Exceptions\HttpException
     * @throws \Pecee\SimpleRouter\Exceptions\NotFoundHttpException
     */
    public static function start(): void
	{
	    /** Default Controllers Namespace */
        SimpleRouter::setDefaultNamespace('\Src\Application\Controllers');

		// Load our helpers
		require_once 'helpers.php';

		// Load our custom routes
		require_once 'routes/web.php';

		// Do initial stuff
		parent::start();
	}

}
