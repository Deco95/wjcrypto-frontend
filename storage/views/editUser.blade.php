@extends('template')
@section('content')
@php
$user = $body;
    if ($body['status_code']){
        $messageTitle = 'Existem erros no formulário‎:';
        $validation= $body['validation'];
    }
@endphp
<!-- Main Content -->
<main class="content">
    <h1 class="title">Editar Perfil</h1>
    @if ($validation)
        <div class="alert alert-info" role="alert">
            {{$messageTitle}}
        </div>
    @endif
    @if ($validation)
        @foreach ($validation as $key => $value)
            <div class="alert alert-danger" role="alert">
                {{$key . ': ' . $value}}
            </div>
        @endforeach
    @endif
    <form action="user" method="post">
        @method('POST')
        <div class="input-field">
            <label for="email"  class="label">E-mail</label>
            <input type="email" id="email" name="email" class="input-text" value="{{$user['email']}}" />
        </div>
        <div class="input-field">
            <label for="password"  class="label">Senha</label>
            <input type="password" id="password" name="password" class="input-text" value="{{$user['password']}}" />
        </div>
        <div class="input-field">
            <label for="name"  class="label">Nome/Razão Social</label>
            <input type="text" id="name" name="name" class="input-text" value="{{$user['name']}}"/>
        </div>
        <div class="input-field">
            <label for="taxvat" class="label">CPF/CNPJ</label>
            <input type="text" id="taxvat"  name="taxvat"  class="input-text" value="{{$user['taxvat']}}" />
        </div>
        <div class="input-field">
            <label for="registry" class="label">RG/Inscrição estatual</label>
            <input type="text" id="registry"  name="registry"  class="input-text" value="{{$user['registry']}}" />
        </div>
        <div class="input-field">
            <label for="birthday" class="label">Data de nascimento/Data fundação</label>
            <input type="text" id="birthday"  name="birthday"  class="input-text" value="{{$user['birthday']}}" />
        </div>
        <div class="input-field">
            <label for="phone" class="label">Telefone</label>
            <input type="text" id="phone"  name="phone"  class="input-text" value="{{$user['phone']}}" />
        </div>
        <div class="input-field">
            <label for="address" class="label">Endereço</label>
            <input type="text" id="address"  name="address"  class="input-text" value="{{$user['address']}}" />
        </div>
        <div class="actions-form">
            <a href="/" class="action back">Voltar</a>
            <input class="btn-submit btn-action"  type="submit" value="Salvar" />
        </div>
    </form>
</main>
<!-- Main Content -->
@stop
