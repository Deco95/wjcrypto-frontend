@extends('template')
@section('content')
    <!-- Main Content -->
    <main class="content">
        <div class="header-list-page">
            <h1 class="title">Products</h1>
            <a href="/addproduct" class="btn-action">Add new Product</a>
        </div>
        <table class="data-grid">
            <tr class="data-row">
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Name</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">SKU</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Price</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Quantity</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Categories</span>
                </th>

                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Actions</span>
                </th>
            </tr>
            @if($products)
                @foreach ($products as $product)
                    <tr class="data-row">
                        <td class="data-grid-td">
                            <span class="data-grid-cell-content">{{ $product->name }}</span>
                        </td>

                        <td class="data-grid-td">
                            <span class="data-grid-cell-content">{{ $product->sku }}</span>
                        </td>

                        <td class="data-grid-td">
                            <span class="data-grid-cell-content">R${{ $product->price }}</span>
                        </td>

                        <td class="data-grid-td">
                            <span class="data-grid-cell-content">{{ $product->quantity }}</span>
                        </td>

                        <td class="data-grid-td">
                            <span class="data-grid-cell-content"> @if($product->categories) @foreach ($product->categories as $category) {{ $category->name }}      <Br /> @endforeach @endif </span>
                        </td>

                        <td class="data-grid-td">
                            <div class="actions">

                                <a class="action edit" href="http://{{url()->getHost()}}/products/{{ $product->id }}">
                                    <span>Edit</span>
                                </a> <br>
                                <a class="action delete" href="http://{{url()->getHost()}}/api/products/{{ $product->id }}/delete">
                                    <span>Delete</span>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endif
        </table>
    </main>
    <!-- Main Content -->
@stop
