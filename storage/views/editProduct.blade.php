@extends('template')
@section('content')
@php
$productCategories = $data->categories->pluck('id')->toArray();
@endphp

<!-- Main Content -->
<main class="content">
    <h1 class="title edit-item">{{ $title }}</h1>

    <form action="http://{{url()->getHost()}}/api/products/{{ $data->id }}/update" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="input-field">
            <label for="sku" class="label">Product SKU</label>
            <input type="text" id="sku" name="sku"  class="input-text" value="{{ $data->sku }}" />
        </div>
        <div class="input-field">
            <label for="name" class="label">Product Name</label>
            <input type="text" id="name" name="name" class="input-text" value="{{ $data->name }}" />
        </div>
        <div class="input-field">
            <label for="price" class="label">Price</label>
            <input type="text" id="price" name="price" class="input-text" value="{{ $data->price }}"  />
        </div>
        <div class="input-field">
            <label for="quantity" class="label">Quantity</label>
            <input type="text" id="quantity" name="quantity" class="input-text" value="{{ $data->quantity }}"  />
        </div>
        <div class="input-field">
            <label for="categories" class="label">Categories</label>
            <select multiple id="categories_id" name ="categories_id[]" class="input-text">
                @if($categories)
                    @foreach ($categories as $category)
                        @if(in_array($category['id'], $productCategories))
                        <option value="{{$category['id']}}" selected>{{$category['name']}}</option>
                        @else
                        <option value="{{$category['id']}}">{{$category['name']}}</option>
                        @endif
                    @endforeach
                @endif
            </select>
        </div>
        <div class="input-field">
            <label for="description" class="label">Description</label>
            <textarea id="description" name="description" class="input-text">{{ $data->description }}</textarea>
        </div>
        <div class="actions-form">
            <a href="http://{{url()->getHost()}}/products" class="action back">Back</a>
            <button class="btn-submit btn-action" type="submit">Save Product</button>
        </div>
    </form>
</main>
<!-- Main Content -->
@stop
