
<!doctype html>
<html ⚡>
<head>
    <title>WJCrypto | {{$title}} </title>
    <!-- Include the Bootstrap 4 theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-bootstrap-4/bootstrap-4.css">

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>
    <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <meta charset="utf-8">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link  rel="stylesheet" type="text/css"  media="all" href="/assets/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
    <meta name="viewport" content="width=device-width,minimum-scale=1">
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script></head>
<!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
    <div class="close-menu">
        <a on="tap:sidebar.toggle">
            <img src="/assets/images/bt-close.png" alt="Close Menu" width="24" height="24" />
        </a>
    </div>
    <a href="/user/home" ><img src="/assets/images/menu-wjcrypto.png" alt="Welcome" width="200" height="43" /></a>
    <div>
        <ul>
            <li><a href="/"  class="link-menu">Logout</a></li>
            <li><a href="/user" class="link-menu">Editar Perfil</a></li>
            <li><a href="/user/home" class="link-menu">WJCrypto</a></li>
        </ul>
    </div>
</amp-sidebar>
<header>
    <div class="go-menu">
        <a on="tap:sidebar.toggle">☰</a>
        <a href="/user/home"  class="link-logo"><img src="/assets/images/wjcrypto-logo.png" alt="Welcome" width="69" height="430" /></a>
    </div>
    <div class="right-box">
        <span class="go-title">{{$title}}</span>
    </div>
</header>
<!-- Header -->

<!-- Main Content -->
@yield('content')
<!-- Main Content -->

<!-- Footer -->
{{--<footer class="footer" style="padding-top: 15px">--}}
{{--    <div class="container footer-image">--}}
{{--        <img src="/assets/images/wjcrypto.png" width="119" height="26" alt="WJCrypto" />--}}
{{--    </div>--}}
{{--    <div class="email-content">--}}
{{--        <span>contato@wjcrypto.com.br</span>--}}
{{--    </div>--}}
{{--</footer>--}}
{{--<footer class="footer">--}}
{{--    <div class="container">--}}
{{--        <span class="text-muted">Place sticky footer content here.</span>--}}
{{--    </div>--}}
{{--</footer>--}}
<!-- Footer --></body>
</html>
