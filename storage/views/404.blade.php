@extends('template')
@section('content')
    @php
        $user = $data;
        $errorCode = 404;
        $codeMessage = 'Page Not Found';
        $message = $body->message ? $body->message :'404';
        $message = $body->message ? $body->message :'Página não encontrada.';

        if ($body['status_code']){
            $errorCode = $body['status_code'];
            $errorMessage = $body['reason_phrase'];
            //$codeMessage = $body['reason_phrase'];
        }

    @endphp
<!-- Main Content -->

{{--<h1 >404: Page Not Found</h1>--}}
<main role="main" class="container">
    <h1 class="mt-5">{{$errorCode}}: {{$codeMessage}}</h1>
    <p class="lead">{{$message}}</p>
    <p class="lead">{{$errorMessage}}</p>
</main>
<!-- Main Content -->
@stop
