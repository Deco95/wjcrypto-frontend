@extends('template')
@section('content')
@php
  $balance;
    if ($body['reason_phrase']){
        $messageTitle = 'Existem erros no formulário‎:';
        $validation= $body['reason_phrase'];
    }
  if ($data['name'])
      $message = 'Bem vindo ' . $body['name'];
@endphp
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">WJCrypto</h1>
    </div>
    <div class="actions-form">
      <div class="col-sm">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Nome/Razão Social</span>
          </div>
          <input disabled type="text" class="form-control" value="{{$body['name']}}" aria-label="name" aria-describedby="basic-addon1">
        </div>

      </div>

      <div class="col-sm">
      </div>
      <div class="col-sm">
        <div class="row"><a class="action back" disabled="true">Saldo: {{$balance}}</a></div>

      </div>
      </div>

    </div>
    @if ($message)
      <div class="alert alert-info" role="alert">
        {{$message}}
      </div>
    @endif
    <h1 class="title new-item">Transações</h1>
    @if ($validation)
      <div class="alert alert-info" role="alert">
        {{$messageTitle}}
      </div>
    @endif
    @if ($validation)
      @foreach ($validation as $key => $value)
        <div class="alert alert-danger" role="alert">
          {{$key . ': ' . $value}}
        </div>
      @endforeach
    @endif

  <!-- Nav tabs -->
    <ul class="nav nav-pills nav-justified" style="">
      <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#home">Depósito</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#menu1">Retirada</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#menu2">Transferência para outro correntista</a>
      </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content" style="padding-top: 30px">
      <div class="tab-pane container active" id="home">
        <form action="/user/credit" method="post">
          @method('POST')
          <div class="form-group">
            <label for="email">Valor:</label>
            <input type="number" name="value" class="form-control" placeholder="0" id="value">
          </div>
          <button type="submit" class="btn btn-primary">Depositar</button>
        </form>
      </div>
      <div class="tab-pane container fade" id="menu1">
        <form action="/user/debit" method="post">
          @method('POST')
          <div class="form-group">
            <label for="email">Valor:</label>
            <input type="number" name="value" class="form-control" placeholder="0" id="value">
          </div>
          <button type="submit" class="btn btn-primary">Retirar</button>
        </form>
      </div>
      <div class="tab-pane container fade" id="menu2">
        <form action="/user/transfer" method="post">
          @method('POST')
          <div class="form-group">
            <label for="email">CPF/CNPJ:</label>
            <input type="text" name="taxvat" class="form-control" placeholder="" id="taxvat">
          </div>
          <div class="form-group">
            <label for="email">Valor:</label>
            <input type="number" name="value" class="form-control" placeholder="0" id="value">
          </div>
          <button type="submit" class="btn btn-primary">Transferir</button>
        </form>
      </div>
    </div>
<!-- Main Content -->
@stop
