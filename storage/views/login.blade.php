@extends('template')
@section('content')
@php
if ($body['name'])
  $message = 'Operação realizada com sucesso, ' . $body['name'];
@endphp
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">WJCrypto</h1>
    </div>
    @if ($message)
      <div class="alert alert-info" role="alert">
        {{$message}}
      </div>
      <div class="alert alert-info" role="alert">
        {{$body['email']}}
      </div>
    @endif
    <h1 class="title new-item">Login</h1>
    <form action="/user/login" method="post">
      @method('POST')
      <div class="input-field">
        <label for="email"  class="label">E-mail</label>
        <input type="email" id="email" name="email" class="input-text" value="user@mail.com.br" />
      </div>
      <div class="input-field">
        <label for="password"  class="label">Senha</label>
        <input type="password" id="password" name="password" class="input-text" value="password" />
      </div>
      <div class="actions-form">
        <a href="/register" class="action back">Registrar</a>
        <input class="btn-submit btn-action"  type="submit" value="Login" />
      </div>
    </form>
  </main>
<!-- Main Content -->
@stop
