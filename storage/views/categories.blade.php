@extends('template')
@section('content')
    <!-- Main Content -->
    <main class="content">
        <div class="header-list-page">
            <h1 class="title">Categories</h1>
            <a href="http://{{url()->getHost()}}/addcategory" class="btn-action">Add new Category</a>
        </div>
        <table class="data-grid">
            <tr class="data-row">
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Name</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Code</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Actions</span>
                </th>
            </tr>
            @if($categories)
                @foreach ($categories as $data)
                    <tr class="data-row">
                        <td class="data-grid-td">
                            <span class="data-grid-cell-content">{{ $data['name'] }}</span>
                        </td>

                        <td class="data-grid-td">
                            <span class="data-grid-cell-content">{{ $data['code'] }}</span>
                        </td>

                        <td class="data-grid-td">
                            <div class="actions">
                                <a class="action edit" href="http://{{url()->getHost()}}/categories/{{ $data['id'] }}">
                                    <span>Edit</span>
                                </a> <br>
                                <a class="action edit" href="http://{{url()->getHost()}}/api/categories/{{ $data['id'] }}/delete">
                                    <span>Delete</span>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endif
        </table>
    </main>
    <!-- Main Content -->
@stop
